
//Se define la funcionalidad con javaScript de las citas.

angular.module('citas', [])
.controller('controllerCitas', function($scope, $http){
    //Función registrar citas.
    $scope.registrarCitas = function(){
        //Creo el formato Json de los datos que voy a reecibir de la cita.
        data = {
            "tipoCita": $scope.tipoCita, //El segundo nombreCita es como se declara en el ng-model.
            "fechaRegistro": $scope.fechaRegistro,
            "fechaCita": $scope.fechaCita,
        }
        //Validemos que los datos a guardar no estén vacios.
        if(data.tipoCita != undefined && data.fechaRegistro != undefined && data.fechaCita != undefined){
            //Si todo está correcto ahora si hago el método para registrar la cita
            $http.post('/registrarCita', data).then(function(data){
                $scope.tipoCita = data.tipoCita;
                $scope.fecha = data.fechaRegistro;
                $scope.fechaCita = data.fechaCita;
            },function(){
                alert("Datos erroneos");
            });
        }
        else{
            alert("Datos Vacios");
        }
    };
    
    //Consultar citas por paciente.
    $scope.consultarCitas = function(){
      //Debo validar que la cedula que estén enviando no esté vacia.
      if($scope.pacienteCedula == undefined || $scope.pacienteCedula == null){
        $scope.pacienteCedula = 0;
      }
      //Conformemos el metodo para consultar todas las citas.
      
      $http.get("/consultarCitasPaciente?cedula=" + $scope.pacienteCedula).success(function(data){
        $scope.filas = data;
      }, function(){
          $scope.filas = [];
          alert("El paciente no tiene citas registradas.");
      })
    };
    
    //ACTUALIZAR CITAS.
   

    //FUNCIONES PARA OCULTAR Y MOSTRAR FORMULARIOS
    function eliminarCita(){
        
    }

});

