
angular.module('citas',[])
    .controller('controllerPaciente', function($scope, $http){
        $scope.registrarPaciente = function(){
            data = {
                "cedula": $scope.cedula,
                "nombre": $scope.nombre,
                "apellido": $scope.apellido,
                "edad": $scope.edad,
                "telefono": $scope.telefono,
                "correo": $scope.correo,
                "direccion": $scope.direccion,
                "usuario": $scope.usuario,
                "contrasenia": $scope.contrasenia,
                "tipoUsuario": $scope.tipoUsuario
            }
            if (data.cedula != undefined && data.nombre != undefined && data.apellido != undefined && data.edad != undefined && data.telefono != undefined && data.correo != undefined && data.direccion != undefined && data.usuario != undefined && data.contrasenia != undefined && data.tipoUsuario != undefined){
                $http.post('/registrarPaciente', data).then(function(data){
                    $scope.cedula = data.cedula;
                    $scope.nombre = data.nombre;
                    $scope.apellido = data.apellido;
                    $scope.edad = data.edad;
                    $scope.telefono = data.telefono;
                    $scope.correo = data.correo;
                    $scope.direccion = data.direccion;
                    $scope.usuario = data.usuario;
                    $scope.contrasenia = data.contrasenia;
                    $scope.tipoUsuario = data.tipoUsuario;
                    alert("Datos registrados satisfactoriamente.");
                },function(){
                    alert("Datos erroneos al registrar el paciente");
                });
            }
            else{
                alert("Los campos no pueden estar vacios, verifiquelos.");
            }
        };
        
        //CONSULTAR TODOS LOS PACIENTES.        
        $scope.consultarPaciente = function(){
            if($scope.pacienteCedula == undefined || $scope.pacienteCedula == null){
                $scope.pacienteCedula = 0;
            }
            
            $http.get("/consultarPaciente?cedula=" + $scope.pacienteCedula).success(function(data){
                $scope.filas = [data];                    
            }, function(){
                $scope.filas = [];
            });   
        };
});

