//JavaScript source code.

angular.module('citas', [])
        .controller('filasCitas', function ($scope, $http) {

            $scope.consultarCitas = function () {
                if ($scope.pacienteCedula === undefined || $scope.pacienteCedula === null) {
                    $scope.pacienteCedula = 0;
                }
                $http.get("/consultarCitasPaciente?cedula=" + $scope.pacienteCedula).success(function (data) {
                    //console.log(data.data);
                    $scope.filas = data;
                }, function () {
                    $scope.filas = [];
                    alert("El paciente no tiene citas registradas");
                });
            };

            //Registrar Paciente;
            $scope.registroPaciente = function () {
                /*if($scope.cedula == undefined || $scope.cedula == null){
                 $scope.cedula = 0;
                 }*/
                data = {
                    "cedula": $scope.cedula,
                    "nombre": $scope.nombre,
                    "apellido": $scope.apellido,
                    "edad": $scope.edad,
                    "telefono": $scope.telefono,
                    "correo": $scope.correo,
                    "direccion": $scope.direccion,
                    "usuario": $scope.usuario,
                    "contrasenia": $scope.contrasenia,
                    "tipoUsuario": $scope.tipoUsuario
                }
                console.log(data);
                if (data.cedula != undefined && data.nombre != undefined && data.apellido != undefined && data.edad != undefined && data.telefono != undefined && data.correo != undefined && data.direccion != undefined && data.usuario != undefined && data.contrasenia != undefined && data.tipoUsuario != undefined) {


                    $http.post('/registrarPaciente', data).then(function (data) {
                        console.log("Entro");

                        $scope.cedula = data.cedula;
                        $scope.nombre = data.nombre;
                        $scope.apellido = data.apellido;
                        $scope.edad = data.edad;
                        $scope.telefono = data.telefono;
                        $scope.correo = data.correo;
                        $scope.direccion = data.direccion;
                        $scope.usuario = data.usuario;
                        $scope.contrasenia = data.contrasenia;
                        $scope.tipoUsuario = data.tipoUsuario;
                        //console.log(data.cedula);

                    }, function () {

                        alert("Datos errones al registrar el paciente.");

                    });
                } else {
                    alert("Datos vacios");
                }
            };

  


        });


/*angular.module('citas',[])
 .controller('filasCitas'), function($scope,$http){
 
 }
 
 // JavaScript source code
 angular.module('citas',[])
 .controller('filasCitas',function($scope,$http){
 
 /*$scope.nombre = "";
 $scope.edad = "";
 $scope.correo = "";
 
 // accion del boton consultar
 $scope.consultar = function(){
 if($scope.cedula == undefined || $scope.cedula == null){
 $scope.cedula = 0;
 }
 
 $http.get("/estudiante?cedula="+$scope.cedula).then(function(data){
 console.log(data.data);
 $scope.nombre = data.data.nombre;
 $scope.edad = data.data.edad;
 $scope.correo = data.data.correo;
 },function(){
 //error
 $scope.nombre = "";
 $scope.edad = "";
 $scope.correo = "";
 $scope.filas = []; // guarda
 });
 
 $http.get("/cuentas?cedula="+$scope.cedula).then(function(data){
 console.log(data.data);
 $scope.filas = data.data;  
 },function(){
 $scope.filas = [];
 });
 };
 
 //acción del botón actualizar
 $scope.actualizar = function(){
 if($scope.cedula == undefined || $scope.cedula == null){
 $scope.cedula = 0;
 }
 data = {
 "id": $scope.cedula,
 "nombre":$scope.nombre,
 "edad": $scope.edad,
 "correo": $scope.correo
 }
 $http.post('/estudiante', data).then(function(data){
 //success
 $scope.nombre = data.data.nombre;
 $scope.edad = data.data.edad;
 $scope.correo = data.data.correo;
 },function(){
 //error
 $scope.nombre = "";
 $scope.edad = "";
 $scope.correo = "";
 $scope.filas = [];
 });
 
 };
 
 
 });*/
