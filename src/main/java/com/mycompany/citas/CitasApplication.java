package com.mycompany.citas;

import com.google.gson.Gson;
import com.mycompany.citas.logica.Cita;
import com.mycompany.citas.logica.Paciente;
import java.awt.PageAttributes;
import java.sql.SQLException;
import java.util.List;

import net.bytebuddy.implementation.bytecode.constant.DefaultValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@SpringBootApplication //Donde se encuentra este arroba indica la pagina principal main.
@RestController //Indica que la clase será un API REST.
public class CitasApplication {
        @Autowired //Perimte la conexión de jdbc template //Nos ahorramos el new a la hora de instanciar un objeto o una clase.
        Cita cita;

        @Autowired
        Paciente paciente;
        //JdbcTemplate jdbcTemplate; //Instancia del jdbctemplate el cual es el equivalente al statement de la clase conexión.
        
        /*
        Ventajas:
        Manejo de excepciones controlados
        Evita el sqlInjection para no permitir ataques de Hackers.
        */
	public static void main(String[] args) {
		SpringApplication.run(CitasApplication.class, args);
	}
        
        //Para trabajar los API de las paginas web, acá vamos a tener todas las vistas de la aplicación.
        @GetMapping("/hello")
        public String hello(@RequestParam(value="name", defaultValue = "World") String name, @RequestParam(value= "edad", defaultValue = "24") String edad){
            
            return String.format("Hello %s, you are %s years old! ", name, edad);
            
        }
         
        @PostMapping("/registrarCita")
        public String registrarCita(@RequestBody String citaEntrante) throws ClassNotFoundException, SQLException{
           
            Cita citas = new Gson().fromJson(citaEntrante, Cita.class);
            cita.setPacienteCedula(citas.getPacienteCedula());
            
            if(cita.consultarTodasCitas() != null){
                cita.setTipoCita(citas.getTipoCita());
                cita.setFechaRegistro(citas.getFechaRegistro());
                cita.setFechaCita(citas.getFechaCita());
                cita.setPacienteCedula(citas.getPacienteCedula());
                cita.guardarCita();
            }
            else{
                System.out.println("El paciente no se encuentra registrado.");
                return null;
            }
            //Paciente pr = new Gson().fromJson(c, Paciente.class);
            
            return new Gson().toJson(cita);
        }
        
        //Consulta todas las citas de un paciente.
        @GetMapping("/consultarCitasPaciente")
        public String consultarCitasPorPaciente(@RequestParam(value = "cedula", defaultValue = "123") String cedula) throws ClassNotFoundException, SQLException{
            cita.setPacienteCedula(cedula);
            List<Cita> citasPaciente = cita.consultarTodasCitas();
            if(citasPaciente.size() > 0){
                return new Gson().toJson(citasPaciente); //Esto me retorna un Json y no tengo necesidad de mandar paramatro a parametro como tipo Json.
                //return "{\"id\":\""+cita.getId+"\"  . .. . .. .  .
            }
            else{
                
                return new Gson().toJson(citasPaciente);
            }           
        }
        
        
        
        
        
        //Consulta todas las citas registradas
        
        
        //Consultar un paciente
        @GetMapping("/consultarPaciente")
        public String consultarPaciente(@RequestParam(value = "cedula", defaultValue = "0") String cedula) throws ClassNotFoundException, SQLException{
            
            paciente.setCedula(cedula);
            
            if(paciente.consultarPaciente()){
                String result = new Gson().toJson(paciente);
                paciente.setCedula("");
                paciente.setNombre("");
                paciente.setApellido("");
                paciente.setEdad(0);
                paciente.setTelefono("");
                paciente.setCorreo("");
                paciente.setDireccion("");
                paciente.setTipoUsuario("");
                return result;
                //return new Gson().toJson(result);
            }
            else{
                //??
                return new Gson().toJson(paciente);
            }
        }
        
        //Consulta todos los pacientes registrados.
        @GetMapping("/consultarPacientes")
        public String consultarPacientes(){
            
            List<Paciente> listarPacientes = paciente.consultarTodosPacientes();
            if(listarPacientes.size() > 0){
                return new Gson().toJson(listarPacientes);
            }
            else{
                return new Gson().toJson(listarPacientes);
            }
        }
        
        //Registrar Paciente.
        @PostMapping("/registrarPaciente")
        public String registrarPaciente(@RequestBody String p) throws ClassNotFoundException, SQLException{
            
            Paciente pacienteReg = new Gson().fromJson(p, Paciente.class);
            
            paciente.setCedula(pacienteReg.getCedula());
            paciente.setNombre(pacienteReg.getNombre());
            paciente.setApellido(pacienteReg.getApellido());
            paciente.setEdad(pacienteReg.getEdad());
            paciente.setTelefono(pacienteReg.getTelefono());
            paciente.setCorreo(pacienteReg.getCorreo());
            paciente.setDireccion(pacienteReg.getDireccion());
            paciente.setUsuario(pacienteReg.getUsuario());
            paciente.setContrasenia(pacienteReg.getContrasenia());
            paciente.setTipoUsuario(pacienteReg.getTipoUsuario());
            paciente.guardarPaciente();
            return new Gson().toJson(paciente);
        }
        
        
        
}

/*

 @PostMapping(path = "/registrarPaciente",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)


paciente.setCedula(cedula); //1007
            if(paciente.consultarPaciente()){
                cita.setPacienteCedula(cedula);
                if(cita.consultarCita()){
                    return "{\"id\":\""+cita.getId()+"\",\"tipoCita\":\""+cita.getTipoCita()+"\",\"fechaRegistro\":\""+cita.getFechaRegistro()+"\",\"fechaCita\":\""+cita.getFechaCita()+
                            "\",\"pacienteCedula\":\""+cita.getPacienteCedula()+"\"}"; //Esto es una estructura de angular.
                            //return new Gson().toJson(cita);
                }
                else{
                    return "{\"id\":\""+cedula+"\",\"cedulaPaciente\":\""+"El paciente no tiene ninguna cita registrada"+"\"}";
                }
            }
            else{
                return "{\"Paciente no se encuentra registrado\"}";
            }


*/